import os
import sys
import numpy as np
import shutil
import cv2
from collections import defaultdict
import random

import json
import os

import numpy as np
import cv2 as cv

def calculate_global_displacement(curr_image, curr_image_bboxes, prev_image=None):
    if prev_image is None:
        return 0.0, 0.0, 0.0

    gray_prev = cv.cvtColor(prev_image, cv.COLOR_BGR2GRAY)

    mask = np.ones_like(gray_prev, dtype=np.uint8)
    for bbox in curr_image_bboxes:
        x, y, w, h = bbox
        mask[y:y+h, x:x+w] = 0  # Set the area inside the bounding box to 0

    feature_params = dict(maxCorners=100, qualityLevel=0.3, minDistance=7, blockSize=7)
    good_features = cv.goodFeaturesToTrack(gray_prev, mask=mask, **feature_params)

    if good_features is None:
        return 0.0, 0.0, 0.0  # No features detected, return 0 displacements

    gray_curr = cv.cvtColor(curr_image, cv.COLOR_BGR2GRAY)

    lk_params = dict(winSize=(15, 15), maxLevel=2, 
                     criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))

    p1, st, err = cv.calcOpticalFlowPyrLK(gray_prev, gray_curr, good_features, None, **lk_params)

    good_new = p1[st == 1]
    good_old = good_features[st == 1]

    displacements = good_new - good_old  # Difference between new and old points

    magnitudes = np.linalg.norm(displacements, axis=1)  # Magnitude of each displacement

    total_displacement = np.sum(magnitudes)  # Total displacement (sum of all displacements)
    average_displacement = np.mean(magnitudes)  # Average displacement
    max_displacement = np.max(magnitudes)  # Maximum displacement
    min_displacement = np.min(magnitudes)  # Minimum displacement

    return total_displacement, max_displacement, min_displacement

def get_trajectories(gt_file):
    f = open(gt_file)
    lines = f.readlines()
    objects_annot, objects_frames = {}, defaultdict(set)
    for line in lines:
        line = line.split(",")
        frame, obj, x, y, w, h = line[0:6]
        objects_frames[obj].add(int(frame))
        objects_annot[(obj, int(frame))] = (int(x), int(y), int(w), int(h))
        
    return objects_annot, objects_frames

def draw_bbox_with_id(image, x, y, w, h, obj_id, color=(0, 255, 0), thickness=2, font_scale=0.5, font_thickness=1):
    cv2.rectangle(image, (x, y), (x + w, y + h), color, thickness)
    label_position = (x + 5, y + 15) 
    cv2.putText(image, str(obj_id), label_position, cv2.FONT_HERSHEY_SIMPLEX, 
                font_scale, color, font_thickness, lineType=cv2.LINE_AA)
    return image

def convert_bbox_to_ltrb_to_ltwth(bbox):
    left, top, right, bottom = bbox
    width = right - left
    height = bottom - top
    return [left, top, width, height]


def split_metadata_jrdb(metadata):
    new_metadata = {"train": {}, "valid": {}, "test": {}}
    train_split = set()
    valid_split = set()
    test_split = set()
    random.seed(42)
    
    for folder, objects in metadata.items():
        objects_list = list(objects)
        random.shuffle(objects_list)  # Shuffle to ensure randomness
        
        train_index = int(len(objects_list) * 0.7)  # 70% for training
        valid_index = int(len(objects_list) * 0.9)  # 20% for validation (cumulative 70 + 20)
        
        train_split = train_split.union(set(objects_list[:train_index]) )
        valid_split = valid_split.union(set(objects_list[train_index:valid_index]) )
        test_split = test_split.union(set(objects_list[valid_index:]) )
    
    new_metadata['train'] = train_split
    new_metadata['valid'] = valid_split
    new_metadata['test'] = test_split

    return new_metadata


def get_objects_for_frames(gt_file):
    f = open(gt_file)
    lines = f.readlines()
    objects_frames = defaultdict(list)
    for line in lines:
        line = line.split(",")
        frame, obj, x, y, w, h = line[0:6]
        objects_frames[frame].append((int(x), int(y), int(w), int(h)))        
    return objects_frames

def split_metadata_mot(metadata):
    new_metadata = {"train": {}, "valid": {}, "test": {}}
    random.seed(42)
    
    for folder in metadata["train"]:
        idxs = metadata["train"][folder]
        random.shuffle(idxs)
        
        train_split_index = int(0.7 * len(idxs))
        valid_split_index = int(0.9 * len(idxs))  # 70% for train + 20% for valid = 90%
    
        train_list = idxs[:train_split_index]
        valid_list = idxs[train_split_index:valid_split_index]
        test_list = idxs[valid_split_index:]
    
        new_metadata["train"][folder] = train_list
        new_metadata["valid"][folder] = valid_list
        new_metadata["test"][folder] = test_list

    return new_metadata

def run_mot():
    DATA_PATH = "input/MOT17/train"
    OUTPUT_PATH = "output/MOT17_output_disp/{}"
    folders = os.listdir(DATA_PATH)
    metadata = {
        "train": {'MOT17-11-DPM': [], 
                  'MOT17-05-DPM': [], 
                  'MOT17-10-DPM': [],
                  'MOT17-09-DPM': []}
    }
    
    
    for folder in metadata["train"]:
        path_to_metadata = os.path.join(DATA_PATH, folder, "gt", "gt.txt")
        with open(path_to_metadata, "r") as file:
            data = file.readlines()
    
        unique_values = set(int(line.split(',')[1]) for line in data)
        metadata["train"][folder] = list(unique_values)
        
    new_metadata = split_metadata_mot(metadata)
    
    
    detectors_folders = os.listdir(DATA_PATH)
    trash = [".DS_Store"]
    
    for each in detectors_folders:
        if each in trash:
            continue
    
        if each not in new_metadata["train"]:
            continue
            
        gt_file = os.path.join(DATA_PATH, each, "gt", "gt.txt")
        img_folder = os.path.join(DATA_PATH, each, "img1")
        
        new_train_img_folder = os.path.join(OUTPUT_PATH.format("train"), each, "img1")
        new_valid_img_folder = os.path.join(OUTPUT_PATH.format("valid"), each, "img1")
        new_test_img_folder = os.path.join(OUTPUT_PATH.format("test"), each, "img1")
        
        frames = os.listdir(img_folder)
        frames.sort()
        objects_annot, objects_frames = get_trajectories(gt_file)
    
        for obj, frames_set in objects_frames.items():
            if int(obj) in new_metadata["train"][each]:
                output_path = OUTPUT_PATH.format("train")
            elif int(obj) in new_metadata["valid"][each]:
                output_path = OUTPUT_PATH.format("valid")
            elif int(obj) in new_metadata["test"][each]:
                output_path = OUTPUT_PATH.format("test")
            else:
                raise ValueError(f"Couldn't find place for obj: {obj}")
            
            obj_folder_path = os.path.join(output_path, each, f"{each}_{obj}")
            os.makedirs(obj_folder_path, exist_ok = True)
            first_frame
            for frame in frames:
                frame_id, _ = os.path.splitext(frame)
                curr_frame = cv2.imread(os.path.join(img_folder, frame))
                if first_frame:
                    total_disp, max_disp, min_disp = calculate_global_displacement(curr_frame, frames_bboxes[int(frame_id)], prev_image=None)
                    first_frame = False
                else:
                    total_disp, max_disp, min_disp = calculate_global_displacement(curr_frame, frames_bboxes[int(frame_id)], prev_image=prev_frame)
                prev_frame = curr_frame
                
                x, y, w, h = objects_annot[(obj, int(frame_id))]
                annot_file = frame_id + ".txt"
                destination_annot_path = os.path.join(obj_folder_path, annot_file)
                
                annot_text = f"{x}, {y}, {w}, {h}, {total_disp}, {max_disp}, {min_disp}"
                f = open(destination_annot_path, "w")
                f.write(annot_text)
                f.close()
    
                frame_path = os.path.join(img_folder, frame)
    
                os.makedirs(new_train_img_folder, exist_ok = True)
                os.makedirs(new_valid_img_folder, exist_ok = True)
                os.makedirs(new_test_img_folder, exist_ok = True)
                
                train_destination_frame_path = os.path.join(new_train_img_folder, frame)
                valid_destination_frame_path = os.path.join(new_valid_img_folder, frame)
                test_destination_frame_path = os.path.join(new_test_img_folder, frame)
                
                shutil.copy(frame_path, train_destination_frame_path)
                shutil.copy(frame_path, valid_destination_frame_path)
                shutil.copy(frame_path, test_destination_frame_path)
                
                #image = cv2.imread(frame_path) 
                #image_with_bbox = draw_bbox_with_id(image, x, y, w, h, obj)
                #cv2.imwrite(destination_frame_path, image_with_bbox)

def run_jrdb():
    DATA_PATH = "input/train_dataset_with_activity"
    OUTPUT_PATH = "output/_output"
    
    random.seed(42)
    
    folders = os.listdir(DATA_PATH)
    available_set = ('image_0', 'image_2', 'image_4', 'image_6')
    available_subset = {
    'image_0': [
        'forbes-cafe-2019-01-22_0',
        'huang-2-2019-01-25_0',
        'meyer-green-2019-03-16_0',
        'gates-159-group-meeting-2019-04-03_0',
        'stlc-111-2019-04-19_0',
        'nvidia-aud-2019-04-18_0',
        'clark-center-intersection-2019-02-28_0',
        'cubberly-auditorium-2019-04-22_0',
        'gates-to-clark-2019-02-28_1',
        'packard-poster-session-2019-03-20_0',
        'clark-center-2019-02-28_1',
        'memorial-court-2019-03-16_0',
        'clark-center-2019-02-28_0',
        'hewlett-packard-intersection-2019-01-24_0',
        'packard-poster-session-2019-03-20_2',
        'huang-basement-2019-01-25_0'
    ],
    'image_2': [
        'tressider-2019-04-26_2'
    ],
    'image_4': [
        'huang-2-2019-01-25_0',
        'clark-center-intersection-2019-02-28_0',
        'clark-center-2019-02-28_1',
        'clark-center-2019-02-28_0'
    ],
    'image_6': [
        'clark-center-intersection-2019-02-28_0',
        'clark-center-2019-02-28_1'
    ]
   }

    
    path_to_metadata_folder = f"{DATA_PATH}/labels/labels_2d"
    metadata_files = os.listdir(path_to_metadata_folder)
    
    images_to_metadata = {}
    for file in metadata_files:
        subfolder_name, folder_name = file.rsplit('_', 1)
        folder_name = folder_name.split(".")[0]
        folder_name = folder_name[:-1] + "_" + folder_name[-1]
        if folder_name not in available_set:
            continue

        if subfolder_name not in available_subset[folder_name]:
            continue
            
        path_to_folders = os.path.join(DATA_PATH, "images", folder_name, subfolder_name) # images inside
        path_to_metadata = os.path.join(DATA_PATH, "labels/labels_2d", file) # images inside
        
        if os.path.exists(path_to_folders):
            images_to_metadata[path_to_folders] = path_to_metadata
    
    
    unique_objects_per_folder = {}
    new_metadata = {"train": {}, "valid": {}, "test": {}}
    
    for images_folder, labels_file in images_to_metadata.items():
        labels_folder_name = os.path.splitext(labels_file)[0].split("/")[-1]
        path_to_objects = os.path.join(OUTPUT_PATH, labels_folder_name)
        unique_objects_per_folder[path_to_objects] = set()
        
        with open(labels_file, "r") as f:
            data = json.load(f)
    
        labels = data["labels"]
        for frame_obj in labels:
            if not os.path.exists(os.path.join(images_folder, frame_obj)):
                print("doesnt' exist: ", os.path.join(images_folder, frame_obj))
                continue
                
            basename = frame_obj.split(".")[0]
            bbox_file_name = basename + ".txt"
    
            for obj in labels[frame_obj]:
                bbox_coord = obj["box"]
                bbox_coord = [int(coord) for coord in bbox_coord]
                bbox_coord = convert_bbox_to_ltrb_to_ltwth(bbox_coord)
                label_id = obj["label_id"]
                label_id = label_id.split(":")[1]
                final_label_id = labels_folder_name + "_" + label_id
                unique_objects_per_folder[path_to_objects].add(final_label_id)
    
    new_metadata = split_metadata_jrdb(unique_objects_per_folder)
    
    
    for images_folder, labels_file in images_to_metadata.items():
        labels_folder_name = os.path.splitext(labels_file)[0].split("/")[-1]
    
        path_to_train_objects = os.path.join(OUTPUT_PATH, "train", labels_folder_name)
        path_to_valid_objects = os.path.join(OUTPUT_PATH, "valid", labels_folder_name)
        path_to_test_objects = os.path.join(OUTPUT_PATH, "test", labels_folder_name)
    
        path_to_train_output_images = os.path.join(path_to_train_objects, "img1")
        path_to_valid_output_images = os.path.join(path_to_valid_objects, "img1")
        path_to_test_output_images = os.path.join(path_to_test_objects, "img1")
    
        os.makedirs(path_to_train_objects, exist_ok = True)
        os.makedirs(path_to_train_output_images, exist_ok = True)
    
        os.makedirs(path_to_valid_objects, exist_ok = True)
        os.makedirs(path_to_valid_output_images, exist_ok = True)
    
        os.makedirs(path_to_test_objects, exist_ok = True)
        os.makedirs(path_to_test_output_images, exist_ok = True)
        
        with open(labels_file, "r") as f:
            data = json.load(f)
            
        for image_name in labels:
            path_to_image = os.path.join(images_folder, image_name)
            if not os.path.exists(path_to_image):
                continue
            curr_image = cv2.imread(path_to_image)
            if len(curr_image) == 0:
                raise ValueError("No image")
            cv2.imwrite(os.path.join(path_to_train_output_images, image_name), curr_image)
            cv2.imwrite(os.path.join(path_to_valid_output_images, image_name), curr_image)
            cv2.imwrite(os.path.join(path_to_test_output_images, image_name), curr_image)
    
        labels = data["labels"]
        for frame_obj in labels:
            if not os.path.exists(os.path.join(images_folder, frame_obj)):
                print("doesnt' exist: ", os.path.join(images_folder, frame_obj))
                continue
                
            basename = frame_obj.split(".")[0]
            bbox_file_name = basename + ".txt"
    
            for obj in labels[frame_obj]:
                bbox_coord = obj["box"]
                bbox_coord = [int(coord) for coord in bbox_coord]
                bbox_coord = convert_bbox_to_ltrb_to_ltwth(bbox_coord)
                label_id = obj["label_id"]
                label_id = label_id.split(":")[1]
                final_label_id = labels_folder_name + "_" + label_id
                
                if final_label_id in new_metadata["train"]:
                    object_path = os.path.join(path_to_train_objects, final_label_id)
                    os.makedirs(object_path, exist_ok = True)
                    
                elif final_label_id in new_metadata["valid"]:
                    object_path = os.path.join(path_to_valid_objects, final_label_id)
                    os.makedirs(object_path, exist_ok = True)
    
                elif final_label_id in new_metadata["test"]:
                    object_path = os.path.join(path_to_test_objects, final_label_id)
                    os.makedirs(object_path, exist_ok = True)
                else:
                    raise ValueError("Unknown label: ", final_label_id)
    
                #print("actual image: ", frame_obj)
                #print("from: ", images_folder)
                #print("into: ", path_to_output_images)
                #print("final_label_id: ", final_label_id)
                path_to_bbox = os.path.join(object_path, bbox_file_name)
                #print("path_to_bbox: ", path_to_bbox)
    
                bbox_coord = [str(coord) for coord in bbox_coord]
                with open(path_to_bbox, "w") as f:
                    f.write(", ".join(bbox_coord))

if __name__ == "__main__":
    run_mot()
    #run_jrdb()
