import numpy as np
import torch
import os
from tqdm import tqdm
import json

from training_lib import SequenceDataset
from transformer_model import TransformerModel
from experimental_lib import prepare_trajectories


def calculate_ade_fde(predicted, ground_truth):
    assert predicted.shape == ground_truth.shape, "Shapes of predicted and ground truth must match."
    distances = np.linalg.norm(predicted - ground_truth, axis=1)
    ade = np.mean(distances)
    fde = distances[-1]
    return ade, fde


def evaluate_model(model, dataloader, device):
    model.eval()
    total_ade, total_fde = 0, 0
    num_sequences = 0

    with torch.no_grad():
        for past_sequence, future_sequence in tqdm(dataloader, desc="Test set iteration"):
            past_result = torch.stack(past_sequence, dim=1).squeeze(0).to(device)
            future_result = torch.stack(future_sequence, dim=1).squeeze(0).to(device)

            predicted = model(past_result).cpu().numpy()
            ground_truth = future_result.cpu().numpy()

            ade, fde = calculate_ade_fde(predicted, ground_truth)
            total_ade += ade
            total_fde += fde
            num_sequences += 1

    mean_ade = total_ade / num_sequences
    mean_fde = total_fde / num_sequences

    print(f"Test Results: Mean ADE = {mean_ade:.4f}, Mean FDE = {mean_fde:.4f}")
    return mean_ade, mean_fde
