import torch
import os
import json

from training_lib import SequenceDataset
from transformer_model import TransformerModel
from experimental_lib import prepare_trajectories
from evaluate_lib import evaluate_model


def main(config_path, models_path, test_data_path, device_type):
    with open(config_path, "r") as f:
        config = json.load(f)

    model_name = config["model_name"]
    input_dim = config["input_dim"]
    output_dim = config["output_dim"]
    padding = config["padding"]
    hidden_dim = config["hidden_dim"]
    num_layers = config["num_layers"]
    nhead = config["nhead"]
    past_sequence_size = config["past_sequence_size"]
    future_sequence_size = config["future_sequence_size"]
    cut_out_test = None

    model = TransformerModel(input_dim=input_dim,
                             hidden_dim=hidden_dim,
                             num_layers=num_layers,
                             nhead=nhead,
                             output_dim=output_dim,
                             future_sequence_size=future_sequence_size).to(torch.device(device_type))

    state_dict = torch.load(os.path.join(models_path,
                                         model_name + ".pth"),
                            map_location=torch.device(device_type))

    model.load_state_dict(state_dict)
    test_set = prepare_trajectories(test_data_path,
                                    past_sequence_size,
                                    future_sequence_size,
                                    padding,
                                    torch.device(device_type),
                                    cut_out_test)

    print('Test length:', len(test_set))
    test_dataloader = SequenceDataset(test_set, steps_per_epochs=len(test_set))

    mean_ade, mean_fde = evaluate_model(model, test_dataloader, torch.device(device_type))
    print("mean_ade: ", mean_ade)
    print("mean_fde: ", mean_fde)


if __name__ == "__main__":
    device_type = "cpu"
    config_path = "models/model_3.json"
    models_path = "models"
    test_data_path = f"output/test_only_with_mot"
    main(config_path, models_path, test_data_path, device_type)
