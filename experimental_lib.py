import torch
import os
from torchvision import models
import cv2
from tqdm import tqdm

from training_lib import normalize_bbox, cut_bbox_with_padding, read_annotations
from transformer_model import extract_resnet_features


def prepare_future_sequence_element(device, annotations):
    image_width, image_height = annotations[:2]
    image_width, image_height = int(image_width), int(image_height)
    bbox = annotations[2:6]
    bbox = [int(el) for el in bbox]
    normalized_bbox = normalize_bbox(bbox, image_width, image_height)
    normalized_bbox = torch.tensor(normalized_bbox).unsqueeze(0)
    normalized_bbox = normalized_bbox.to(device)

    return normalized_bbox


def prepare_past_sequence_element(device, visual_fs, annotations):
    image_width, image_height = annotations[:2]
    image_width, image_height = int(image_width), int(image_height)
    bbox = annotations[2:6]
    bbox = [int(el) for el in bbox]
    extra_parameters = []
    if len(annotations) > 6:
        extra_parameters = annotations[6:]
        extra_parameters = [el.split(".")[0] for el in extra_parameters]
        extra_parameters = [el for el in extra_parameters if el != ""]
        extra_parameters = [int(el) for el in extra_parameters]

    normalized_bbox = normalize_bbox(bbox, image_width, image_height)
    normalized_bbox = torch.tensor(normalized_bbox).unsqueeze(0)
    normalized_bbox = normalized_bbox.to(device)

    if extra_parameters:
        extra_parameters = torch.tensor(extra_parameters).unsqueeze(0)
        extra_parameters = extra_parameters.to(device)
        final_features = torch.cat((visual_fs, normalized_bbox, extra_parameters), dim=1)
    else:
        final_features = torch.cat((visual_fs, normalized_bbox), dim=1)

    return final_features


def generate_future_sequences(sequence, past, future, device, cut_out):
    result = []
    n = len(sequence)
    for i in tqdm(range(n - past - future + 1), desc="Creating trajectories"):
        if cut_out and len(result) == cut_out:
            return result
        past_sequence = sequence[i: i + past]
        future_sequence = sequence[i + past: i + past + future]

        past_sequence = [
            prepare_past_sequence_element(device=device, visual_fs=visual_fs, annotations=annotations).to(device) for
            (visual_fs, annotations) in past_sequence]
        future_sequence = [prepare_future_sequence_element(device=device, annotations=annotations).to(device) for
                           (visual_fs, annotations) in future_sequence]
        result.append((past_sequence, future_sequence))
    return result


def prepare_trajectories(data_path, past_sequence_size, future_sequence_size, padding, device, cut_out):
    black_list = ['.DS_Store', "img1"]
    resnet = models.resnet50(pretrained=True)
    resnet = torch.nn.Sequential(*(list(resnet.children())[:-1]))
    resnet = resnet.to(device)
    resnet.eval()

    folders = os.listdir(data_path)
    total_trajectories = []
    for each in folders:
        if each in black_list:
            continue
        objects = os.listdir(os.path.join(data_path, each))
        images_sequence = os.listdir(os.path.join(data_path, each, "img1"))
        read_images_sequence = {}
        for image_name in tqdm(images_sequence, desc=f"Reading images for {each}"):
            image_path = os.path.join(data_path, each, "img1", image_name)
            image = cv2.imread(image_path)
            read_images_sequence[image_name] = image

        for obj in tqdm(objects, desc=f"Reading objects for {each}"):
            if obj in black_list:
                continue
            general_sequence = sorted(os.listdir(os.path.join(data_path, each, obj)))
            images_sequence = []
            for item in general_sequence:
                if os.path.splitext(item)[1] == ".txt":
                    try:
                        basename = os.path.splitext(item)[0]
                        image_name = basename + ".jpg"
                        bbox_path = os.path.join(data_path, each, obj, basename + ".txt")
                        annotations = read_annotations(bbox_path)
                        image_width, image_height = annotations[:2]
                        image_width, image_height = int(image_width), int(image_height)

                        bbox = annotations[2:6]
                        bbox = [int(el) for el in bbox]
                        cut_image = cut_bbox_with_padding(read_images_sequence[image_name], bbox, padding)
                        img_rgb = cv2.cvtColor(cut_image, cv2.COLOR_BGR2RGB)
                        visual_features = extract_resnet_features(img_rgb, resnet, image_width, image_height)
                        images_sequence.append([visual_features, annotations])
                    except Exception:
                        break
            if len(images_sequence) > 0:
                trajectories = generate_future_sequences(sequence=images_sequence,
                                                         past=past_sequence_size,
                                                         future=future_sequence_size,
                                                         device=device,
                                                         cut_out=cut_out)
                total_trajectories.extend(trajectories)
            else:
                with open("skipped_objects.txt", "a") as file:
                    file.write(f"obj={obj} in path {os.path.join(data_path, each, obj)}\n")

    return total_trajectories


def _train(model, epochs, train_dataloader, valid_dataloader, criterion, optimizer):
    train_losses, valid_losses = [], []
    for _ in tqdm(range(epochs), desc="Epochs iteration..."):
        model.train()
        train_steps_in_batch = 0

        avg_train_loss, avg_valid_loss = 0, 0
        for batch_idx, (past_sequence, future_sequence) in tqdm(enumerate(train_dataloader),
                                                                desc="Train set iteration"):
            past_result = torch.stack(past_sequence, dim=1).squeeze(0)
            future_result = torch.stack(future_sequence, dim=1).squeeze(0)

            outputs = model(past_result)
            loss = criterion(outputs, future_result)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            avg_train_loss += loss.item()
            train_steps_in_batch += 1

        train_dataloader.on_epoch_end()

        model.eval()
        valid_steps_in_batch = 0
        with torch.no_grad():
            for batch_idx, (past_sequence, future_sequence) in tqdm(enumerate(valid_dataloader),
                                                                    desc="Valid set iteration"):
                past_result = torch.stack(past_sequence, dim=1).squeeze(0)
                future_result = torch.stack(future_sequence, dim=1).squeeze(0)

                outputs = model(past_result)
                loss = criterion(outputs, future_result)
                avg_valid_loss += loss.item()
                valid_steps_in_batch += 1

        train_loss_avg = avg_train_loss / train_steps_in_batch
        train_losses.append(train_loss_avg)

        valid_loss_avg = avg_valid_loss / valid_steps_in_batch
        valid_losses.append(valid_loss_avg)
    return train_losses, valid_losses
