import torch
import torch.nn as nn
from torchvision import transforms


class TransformerModel(nn.Module):
    def __init__(self, input_dim, hidden_dim, num_layers, nhead, output_dim, future_sequence_size):
        super(TransformerModel, self).__init__()

        self.embedding_src = nn.Linear(input_dim, hidden_dim)
        self.positional_encoding = PositionalEncoding(hidden_dim)

        self.transformer_encoder = nn.TransformerEncoder(
            nn.TransformerEncoderLayer(d_model=hidden_dim, nhead=nhead),
            num_layers=num_layers
        )

        self.fc1 = nn.Linear(hidden_dim, hidden_dim)
        self.fc2 = nn.Linear(hidden_dim, hidden_dim // 2)
        self.fc_out = nn.Linear(hidden_dim //2 , output_dim)
        
        self.sigmoid = nn.Sigmoid()
        self.future_sequence_size = future_sequence_size

    def forward(self, src):
        embedded_src = self.embedding_src(src) 
        #embedded_src = self.positional_encoding(embedded_src.unsqueeze(1))
        transformer_output = self.transformer_encoder(embedded_src)

        x = self.fc1(transformer_output)
        x = nn.ReLU()(x)
        x = self.fc2(x)
        x = nn.ReLU()(x)
        output = self.fc_out(x)
        
        output = self.sigmoid(output)
        return output.squeeze(1)[:self.future_sequence_size]


def extract_resnet_features(img_rgb, resnet, image_width, image_height):
    image_width, image_height = int(image_width), int(image_height)
    preprocess = transforms.Compose([
        transforms.ToPILImage(), 
        transforms.Resize((image_width, image_height)),
        transforms.ToTensor()
    ])
    
    img_tensor = preprocess(img_rgb).unsqueeze(0)
    device = next(resnet.parameters()).device
    img_tensor = img_tensor.to(device)
    
    with torch.no_grad():  
        features = resnet(img_tensor)
    
    features = features.view(features.size(0), -1)
    return features


class PositionalEncoding(nn.Module):
    def __init__(self, hidden_dim, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.encoding = torch.zeros(max_len, hidden_dim)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, hidden_dim, 2).float() * (-torch.log(torch.tensor(10000.0)) / hidden_dim))
        self.encoding[:, 0::2] = torch.sin(position * div_term)
        self.encoding[:, 1::2] = torch.cos(position * div_term)
        self.encoding = self.encoding.unsqueeze(0)

    def forward(self, x):
        return x + self.encoding[:, :x.size(1), :].to(x.device)
