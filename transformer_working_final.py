import torch
import torch.nn as nn
import torch.optim as optim
import os
import json
import argparse
import dill
# [1, 4, t, 2052]

from training_lib import create_plot, create_metadata, SequenceDataset
from transformer_model import TransformerModel
from experimental_lib import prepare_trajectories, _train


def main(model_name, config_path):
    with open(config_path, "r") as f:
        config = json.load(f)

    models_path = "models"
    os.makedirs(models_path, exist_ok=True)
    
    input_dim = config["input_dim"]
    output_dim = config["output_dim"]
    padding = config["padding"]
    hidden_dim = config["hidden_dim"]
    num_layers = config["num_layers"]
    nhead = config["nhead"]
    past_sequence_size = config["past_sequence_size"]
    future_sequence_size = config["future_sequence_size"]
    cut_out_train = config["cut_out_train"]
    cut_out_valid = config["cut_out_valid"]
    steps_per_epochs = config["steps_per_epochs"]
    epochs = config["epochs"]
    learning_rate = config["learning_rate"]
    general_path = config["training_data_path"]

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = TransformerModel(input_dim=input_dim, hidden_dim=hidden_dim, num_layers=num_layers, nhead=nhead, output_dim=output_dim, future_sequence_size=future_sequence_size).to(device)
    
    criterion = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
    
    train_data_path = f"{general_path}/train"
    valid_data_path = f"{general_path}/valid"
    
    train_set = prepare_trajectories(train_data_path, past_sequence_size, future_sequence_size, padding, device, cut_out_train)
    valid_set = prepare_trajectories(valid_data_path, past_sequence_size, future_sequence_size, padding, device, cut_out_valid)
    print('Train length:', len(train_set))
    print('Valid length:', len(valid_set))
    
    train_dataloader = SequenceDataset(train_set, steps_per_epochs=steps_per_epochs)
    valid_dataloader = SequenceDataset(valid_set, steps_per_epochs=len(valid_set))
    
    train_losses, valid_losses = _train(model=model,
                                        epochs=epochs,
                                        train_dataloader=train_dataloader,
                                        valid_dataloader=valid_dataloader,
                                        criterion=criterion,
                                        optimizer=optimizer)
    print("Training complete!")
    create_plot(model_name, epochs, train_losses, valid_losses)
    
    torch.save(model, os.path.join(models_path, model_name + ".pth"), pickle_module=dill)
    create_metadata(model_name, 
                    input_dim, 
                    output_dim, 
                    hidden_dim, 
                    padding, 
                    num_layers, 
                    nhead, 
                    learning_rate, 
                    epochs, 
                    future_sequence_size,
                    past_sequence_size,
                    cut_out_train,
                    cut_out_valid,
                    steps_per_epochs)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process some model parameters.")
    parser.add_argument('--model_name', type=str, required=True, help="Name of the model to use")
    args = parser.parse_args()
    model_name = args.model_name
    print("Model name:", model_name)
    config_path = "config.json"

    main(model_name, config_path)
