import random
from torch.utils.data import Dataset
import matplotlib.pyplot as plt
import cv2
import json
from pathlib import Path


def replace_last_dir(path, new_dir_name):
    file_path = Path(path)
    new_path = file_path.parent.parent / new_dir_name / file_path.name
    return new_path

def denormalize_bbox(bbox_norm, image_width, image_height):
    image_width, image_height = int(image_width), int(image_height)
    x_norm, y_norm, w_norm, h_norm = bbox_norm
    x = x_norm * image_width
    y = y_norm * image_height
    w = w_norm * image_width
    h = h_norm * image_height
    return (x, y, w, h)

def normalize_bbox(bbox, image_width, image_height):
    image_width, image_height = int(image_width), int(image_height)
    x, y, w, h = bbox
    x_norm = x / image_width
    y_norm = y / image_height
    w_norm = w / image_width
    h_norm = h / image_height
    return (x_norm, y_norm, w_norm, h_norm)


def cut_bbox_with_padding(image, bbox, padding):
    x, y, w, h = bbox
    pad_left, pad_top, pad_right, pad_bottom = padding
    x_min_padded = max(x - pad_left, 0)
    y_min_padded = max(y - pad_top, 0)
    x_max_padded = min(x + w + pad_right, image.shape[1])
    y_max_padded = min(y + h + pad_bottom, image.shape[0])
    cropped_image = image[y_min_padded:y_max_padded, x_min_padded:x_max_padded]
    return cropped_image

def read_annotations(bbox_path):
    file = open(bbox_path, "r")
    text = file.read()
    annotations = text.split(",")
    return annotations

def draw_bbox_on_image(image, bbox_tensor, color=(0, 255, 0), thickness=2):
    x, y, w, h = bbox_tensor
    x1, y1 = int(x), int(y)
    x2, y2 = int(x + w), int(y + h)
    cv2.rectangle(image, (x1, y1), (x2, y2), color, thickness)
    return image


class SequenceDataset(Dataset):
    def __init__(self, sequence_pairs, steps_per_epochs, batch_size=1,  shuffle=True):
        self.sequence_pairs = sequence_pairs
        self.sub_sequence_pairs = self.sequence_pairs [:steps_per_epochs]
        self.shuffle = shuffle
        self.batch_size = batch_size
        self.steps_per_epochs = steps_per_epochs

    def __len__(self):
        return len(self.sub_sequence_pairs)

    def __getitem__(self, idx):
        past_sequence, future_sequence = self.sub_sequence_pairs[idx]
        return past_sequence, future_sequence

    def on_epoch_end(self):
        if self.shuffle:
            random.seed(42) 
            random.shuffle(self.sequence_pairs)
            self.sub_sequence_pairs = self.sequence_pairs [:self.steps_per_epochs]


def create_plot(model_name, epochs, train_losses, valid_losses):
    plt.figure(figsize=(10, 5))
    plt.plot(range(1, epochs + 1), train_losses, label='Training Loss', marker='o')
    plt.plot(range(1, epochs + 1), valid_losses, label='Validation Loss', marker='v')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.title('Training and Validation Loss Over Epochs')
    plt.legend()
    plt.grid(True)
    plt.savefig(model_name + '.png', bbox_inches='tight', dpi=300)
    plt.close()


def create_metadata(model_name, 
                    input_dim, 
                    output_dim, 
                    hidden_dim, 
                    padding, 
                    num_layers, 
                    nhead, 
                    learning_rate, 
                    epochs, 
                    future_sequence_size,
                    past_sequence_size,
                    cut_out_train,
                    cut_out_valid,
                    steps_per_epochs):
    config = {
        "model_name": model_name,
        "input_dim": input_dim,
        "output_dim": output_dim,
        "hidden_dim": hidden_dim,
        "padding": padding,
        "num_layers": num_layers,
        "nhead": nhead,
        "learning_rate": learning_rate,
        "epochs": epochs,
        "future_sequence_size": future_sequence_size,
        "past_sequence_size": past_sequence_size,
        "cut_out_train": cut_out_train,
        "cut_out_valid": cut_out_valid,
        "steps_per_epochs": steps_per_epochs
    }

    with open(model_name + ".json", "w") as json_file:
        json.dump(config, json_file, indent=4)

